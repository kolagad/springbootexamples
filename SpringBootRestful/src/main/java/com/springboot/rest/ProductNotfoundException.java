/**
 * 
 */
package com.springboot.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author skolagad
 *
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProductNotfoundException extends RuntimeException {
	   private static final long serialVersionUID = 1L;
	   

		public ProductNotfoundException(String message) {
			super(message);
		}
	}
