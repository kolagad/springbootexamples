/**
 * 
 */
package com.springboot.rest;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author skolagad
 *
 */
@RestController
@EnableSwagger2
@EnableAdminServer
public class ProductServiceController {
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(ProductServiceController.class);
	   
	private static Map<String, Product> productRepo = new HashMap<>();
	static {
		Product honey = new Product();
		honey.setId("1");
		honey.setName("Honey");
		productRepo.put(honey.getId(), honey);

		Product almond = new Product();
		almond.setId("2");
		almond.setName("Almond");
		productRepo.put(almond.getId(), almond);
	}

	@RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable("id") String id) {
		logger.info("Entered delete method");
		productRepo.remove(id);
		logger.info("Completed delete method");
		return new ResponseEntity<>("Product is deleted successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/products/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Product product) {
		logger.info("Entered delete method");
		if(!productRepo.containsKey(id))throw new ProductNotfoundException("id not found");
		productRepo.remove(id);
		product.setId(id);
		productRepo.put(id, product);
		logger.info("Completed update method");
		return new ResponseEntity<>("Product is updated successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/products", method = RequestMethod.POST)
	public ResponseEntity<Object> createProduct(@RequestBody Product product) {
		logger.info("Entered create method");
		productRepo.put(product.getId(), product);
		logger.info("Completed create method");
		return new ResponseEntity<>("Product is created successfully", HttpStatus.CREATED);
	}

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ResponseEntity<Object> getProduct() {
		logger.info("Entered get method");
		logger.info("Completed get method");
		return new ResponseEntity<>(productRepo.values(), HttpStatus.OK);
	}
	
	@Bean
	   public Docket productApi() {
	      return new Docket(DocumentationType.SWAGGER_2).select()
	         .apis(RequestHandlerSelectors.basePackage("com.springboot.rest;")).build();
	   }
}
